## Canvas / Slack Group Number

Group 11

## Team Members

Sugandha Kumar, Adithya Bhonsley, Benjamin Sandoval, Anna Jimenea, Jean-Claude Bissou

## Project Name

SupportSouthSudan

## Website Link

https://www.supportsouthsudan.me/

## Develop Website Link

https://develop.d1b4k40wgfxdel.amplifyapp.com/

## Git SHA

f47392707acde77027ba12fab8a961081a218d93

## Project Description

SupportSouthSudan aims to create an understanding of the South Sudanese refugees, including the countries that have accepted them, associated charities/organizations, and related news articles. The goal is to increase awareness about the refugees' experiences, highlight the crucial role played by charities in assisting their community, and provide up-to-date information on the country's refugee situation.

## Phase Leader

Leader: Jean-Claude Bissou
Responsiblities: setting up repo, issue board, acquiring domain, hosting front-end, team time management

Leader: Anna Jimenea
Responsiblities: Scheduling meetings, assuring rubric was followed, assigning issues, various assistance

## Estimated Time Working

Sugandha Kumar: 18 hours
Adithya Bhonsley: 10 hours
Benjamin Sandoval: 12 hours
Anna Jimenea: 16 hours
Jean-Claude Bissou: 20 hours

Sugandha Kumar: 20 hours
Adithya Bhonsley: 10 hours
Benjamin Sandoval: 20 hours
Anna Jimenea: 20 hours
Jean-Claude Bissou: 25 hours

## Actual Time Working

Sugandha Kumar: 24 hours
Adithya Bhonsley: 12 hours
Benjamin Sandoval: 20 hours
Anna Jimenea: 27 hours
Jean-Claude Bissou: 30 hours

Sugandha Kumar: 24 hours
Adithya Bhonsley: 15 hours
Benjamin Sandoval: 14 hours
Anna Jimenea: 26 hours
Jean-Claude Bissou: 35 hours

## Postman API

https://documenter.getpostman.com/view/32889511/2s9Yyzde1E

## Data Sources

- UNHCR ODP https://data.unhcr.org/en/situations/southsudan
- ReliefWeb https://reliefweb.int/updates?view=headlines&search=south+sudan+refugee
- UNHCR Data Finder https://www.unhcr.org/refugee-statistics/download/?url=jwA88D
- UNHCR ODP API https://data.unhcr.org/api/doc#/
- REST Countries https://restcountries.com/
- News API https://newsapi.org/
- Youtube API https://developers.google.com/youtube/v3/
- Bing API https://www.microsoft.com/en-us/bing/apis/bing-web-search-api
- Google Maps API https://developers.google.com/maps

## Models

- Countries
- News
- Charities

## Estimated Number of Instances per Model

- Countries: 55
- News: 644
- Charities: ~100

## Attributes for Each Model

- Countries:

  - Name
  - Pop. of accepted refugees from South Sudan
  - Pop. of asylum-seekers from South Sudan
  - Total Population
  - Distance from South Sudan? (Likely)

- News:
  - Article name
  - Publishing date
  - News Source
  - Country of origin/importance
  - Type of article (press release, analysis, etc.)
  - Link to the article
- Charities:
  - Name
  - Type of assistance (Food, medical, education, etc.)
  - Location
  - Establishment date
  - Donations/volunteers
  - Amount (monetarily) contributed

## Model Connections

- Countries - News: News articles involving this country
- Countries - Charities: Countries that the charities originate from
- News - Country: What country the news takes place in
- News - Charities: Charities that serve as the source of the article
- Charities - Countries: Countries the charities originate from
- Charities - News: News articles that cover the same topic this charity seeks to help with

## Media for Each Model

- Countries:
  - Map (showing distribution) or graph
  - Text
- News:
  - Image/News Clipping
  - Text
- Charities:
  - Image (Flyer/Poster)
  - Video
  - Text

## Questions Our Website Will Answer

- What is currently happening in South Sudan as of recently?
- What countries do people who flee from South Sudan seek asylum?
- What resources or organizations can provide support for South Sudanese people once they find asylum?

## Database Diagram

https://drive.google.com/file/d/1Wr5uIyzEUltJGG3O4ujVS-i5YVPyYrw3/view?usp=sharing