import React from 'react';
import { render, screen, fireEvent, waitFor } from '@testing-library/react';
import { BrowserRouter as Router } from 'react-router-dom';
import App from '../src/App';
import ResizeObserver from 'resize-observer-polyfill';
global.ResizeObserver = ResizeObserver;

jest.mock('@react-google-maps/api', () => ({
  GoogleMap: jest.fn(() => null), // Mock GoogleMap component
}));

require('canvas');

describe('App component', () => {

  beforeEach(() => {
      render(
          <App />
      );
  });

  test('renders navigation links correctly', () => {
    const navLinks = screen.getAllByRole('link');
    expect(navLinks).toHaveLength(6);
  });  

  test('navigates to home page by default', () => {
    const homePageTitle = screen.getByRole('heading', { name: /Our Purpose/i });
    expect(homePageTitle).toBeTruthy();
  });

  test('navigates to any page regardless of where it is', async () => {
    const homePageTitle = screen.getByRole('heading', { name: /Our Purpose/i });
    expect(homePageTitle).toBeTruthy();

    const aboutLinkInNav = screen.getByRole('link', { name: /about/i });
    fireEvent.click(aboutLinkInNav);
    const aboutPageTitle = await screen.findByRole('heading', { name: /about/i });
    expect(aboutPageTitle).toBeTruthy();
    
    const countriesLinkInNav = screen.getByRole('link', { name: /countries/i });
    fireEvent.click(countriesLinkInNav);
    const countriesPageTitle = await screen.findByRole('heading', { name: /countries/i });
    expect(countriesPageTitle).toBeTruthy();

    const organizationsLinkInNav = screen.getByRole('link', { name: /organizations/i });
    fireEvent.click(organizationsLinkInNav);
    const organizationsPageTitle = await screen.findByRole('heading', { name: /organizations/i }, { timeout:10000 });
    expect(organizationsPageTitle).toBeTruthy();
  });


  test('renders countries page with correct data when countries link is clicked', async () => {
    const countriesLink = screen.getByRole('link', { name: /countries/i });
    fireEvent.click(countriesLink);

    const countriesPageTitle = await screen.findByRole('heading', { name: /countries/i });
    expect(countriesPageTitle).toBeTruthy();

    const countryModelCards = screen.getAllByTestId('country-model-card');
    // change to 50 later
    expect(countryModelCards.length).toBeGreaterThanOrEqual(6);
    
    const counter = screen.queryByTestId('instance-counter') as HTMLElement | null;
    expect(counter?.textContent).toBeTruthy(); 
    const instanceCount = parseInt(counter?.textContent?.match(/\d+/)?.[0] ?? '0');
    // change to 50 later
    expect(instanceCount).toBeGreaterThanOrEqual(6);

    const prevButton = screen.getByText('Prev');
    expect(prevButton).toBeTruthy();
    const nextButton = screen.getByText('Next');
    expect(nextButton).toBeTruthy();
  });  
  

  test('renders Sudan country instance page when clicked', async () => {
    const countriesLink = screen.getByRole('link', { name: /countries/i });
    expect(countriesLink).toBeTruthy();
    fireEvent.click(countriesLink);
    const sudanLinks = await waitFor(() => screen.getByText(/Afghanistan/i, { selector: '.card-title' }), { timeout: 10000 });
    fireEvent.click(sudanLinks);
    const pageTitle = screen.getByText(/Afghanistan/i, { selector: '.text-center.mb-4' });
    expect(pageTitle).toBeTruthy();

    const populationAccepted = screen.getByText(/Population accepted refugees/i);
    expect(populationAccepted).toBeTruthy();
    const populationSeekers = screen.getByText(/Population asylum seekers/i);
    expect(populationSeekers).toBeTruthy();
    const totalPopulation = screen.getByText(/Total country population/i);
    expect(totalPopulation).toBeTruthy();
  }, 15000);

  test('has links to other pages', async () => {
    const charityLinks = screen.getAllByRole('link', { name: /organizations/i });
    expect(charityLinks.length).toBeTruthy();

    const newsLinks = screen.getAllByRole('link', { name: /news/i });
    expect(newsLinks.length).toBeTruthy();

    const images = screen.getAllByRole('img');
    expect(images.length).toBeTruthy();
  });

  test('navigates back to other pages from Sudan instance page', async () => {
    const countriesLink = screen.getByRole('link', { name: /countries/i });
    fireEvent.click(countriesLink);
    const sudanLinks = await waitFor(() => screen.getByText(/Afghanistan/i, { selector: '.card-title' }), { timeout: 10000 });
    fireEvent.click(sudanLinks);

    const homeLink = screen.getByRole('link', { name: /supportsouthsudan/i });
    fireEvent.click(homeLink);
    const homePageTitle = await screen.findByRole('heading', { name: /Our Purpose/i });
    expect(homePageTitle).toBeTruthy();

    const aboutLinkInNav = screen.getByRole('link', { name: /about/i });
    fireEvent.click(aboutLinkInNav);
    const aboutPageTitle = await screen.findByRole('heading', { name: /about/i });
    expect(aboutPageTitle).toBeTruthy();
    
    const countriesLinkInNav = screen.getByRole('link', { name: /countries/i });
    fireEvent.click(countriesLinkInNav);
    const countriesPageTitle = await screen.findByRole('heading', { name: /countries/i });
    expect(countriesPageTitle).toBeTruthy();

    const organizationsLinkInNav = screen.getByRole('link', { name: /organizations/i });
    fireEvent.click(organizationsLinkInNav);
    const organizationsPageTitle = await screen.findByRole('heading', { name: /organizations/i }, { timeout:10000 });
    expect(organizationsPageTitle).toBeTruthy();

    const newsLinkInNav = screen.getByRole('link', { name: /news/i });
    fireEvent.click(newsLinkInNav);
    const newsPageTitle = await screen.findByRole('heading', { name: /news/i }, { timeout:10000 });
    expect(newsPageTitle).toBeTruthy();
  }, 20000);


  test('renders organizations page with correct data when organizations link is clicked', async () => {
    const organizationsLink = screen.getByRole('link', { name: /organizations/i });
    fireEvent.click(organizationsLink);

    const organizationsPageTitle = await screen.findByRole('heading', { name: /organizations/i }, {timeout:20000});
    expect(organizationsPageTitle).toBeTruthy();

    const charityModelCards = await waitFor(() => screen.getAllByTestId('charity-model-card'), { timeout: 20000 });
    expect(charityModelCards.length).toBeGreaterThanOrEqual(6);
    
    const counter = await waitFor(() => screen.queryByTestId('instance-counter') as HTMLElement | null);
    expect(counter?.textContent).toBeTruthy(); 
    const instanceCount = parseInt(counter?.textContent?.match(/\d+/)?.[0] ?? '0');
    expect(instanceCount).toBeGreaterThanOrEqual(6);

    const prevButton = screen.getByText('Prev');
    expect(prevButton).toBeTruthy();
    const nextButton = screen.getByText('Next');
    expect(nextButton).toBeTruthy();
  }, 40000);  

  test('renders Internews instance page when clicked', async () => {
    const charitiesLink = screen.getByRole('link', { name: /organizations/i });
    fireEvent.click(charitiesLink);

    await waitFor(() => {
      const unhcrLinks = screen.getAllByText(/Internews/i, { selector: '.card-title' }); 
      const randomIndex = Math.floor(Math.random() * unhcrLinks.length);
      fireEvent.click(unhcrLinks[randomIndex]);
    });

    const pageTitle = screen.getByText(/Internews/i, { selector: '.mb-4' });
    expect(pageTitle).toBeTruthy();

    const logoImageElement = screen.getByAltText('Logo');
    expect(logoImageElement).toBeTruthy();

  const assistanceTypeElement = screen.getByText(/Status:/i);
  expect(assistanceTypeElement).toBeTruthy();

  const locationElement = screen.getByText(/Headquarters Location:/i);
  expect(locationElement).toBeTruthy();

  const establishDateElement = screen.getByText(/Date of Establishment:/i);
  expect(establishDateElement).toBeTruthy();

  const donoOrVolunElement = screen.getByText(/Type of organization:/i);
  expect(donoOrVolunElement).toBeTruthy();
});

  test('navigates back to other pages from organizations page', async () => {
    const organizationsLink = screen.getByRole('link', { name: /organizations/i });
    fireEvent.click(organizationsLink);

    const homeLink = screen.getByRole('link', { name: /supportsouthsudan/i });
    fireEvent.click(homeLink);
    const homePageTitle = await screen.findByRole('heading', { name: /Our Purpose/i });
    expect(homePageTitle).toBeTruthy();

    const aboutLinkInNav = screen.getByRole('link', { name: /about/i });
    fireEvent.click(aboutLinkInNav);
    const aboutPageTitle = await screen.findByRole('heading', { name: /about/i });
    expect(aboutPageTitle).toBeTruthy();
  });

});