import React from 'react';
import { render, screen, fireEvent, waitFor } from '@testing-library/react';
import { BrowserRouter as Router } from 'react-router-dom';
import App from '../src/App';
import ResizeObserver from 'resize-observer-polyfill';
global.ResizeObserver = ResizeObserver;

// import userEvent from '@testing-library/user-event';
jest.mock('@react-google-maps/api', () => ({
  GoogleMap: jest.fn(() => null), // Mock GoogleMap component
}));

describe('App component', () => {
  beforeEach(() => {
    render(
      <App />
    );
  });

  test('renders without crashing', () => {
    // No need to render App here, as it's already rendered in beforeEach
    // Test logic goes here
  });

  test('renders the navbar', () => {
    const navbarElement = screen.getByRole('navigation');
    expect(navbarElement).toBeTruthy();
  });

  test('renders the home page by default', () => {
    const homePageElement = screen.getByText(/Our Purpose/i);
    expect(homePageElement).toBeTruthy();
  });  

  test('renders the about page', () => {
    const aboutLink = screen.getByRole('link', { name: /about/i });
    expect(aboutLink).toBeTruthy();
    fireEvent.click(aboutLink);
    const aboutPageElement = screen.getByText('Meet the Team');
    expect(aboutPageElement).toBeTruthy();
  });

  test('renders the countries page', async () => {
    const countriesLink = screen.getByRole('link', { name: /countries/i });
    expect(countriesLink).toBeTruthy();
    fireEvent.click(countriesLink);
    const pageTitle = await screen.findByRole('heading', { name: /countries/i });
    expect(pageTitle).toBeTruthy();
  });

  test('renders the Organizations page', async () => {
    const organizationsLink = screen.getByRole('link', { name: /organizations/i });
    expect(organizationsLink).toBeTruthy();
    fireEvent.click(organizationsLink);
    const pageTitle = await screen.findByRole('heading', { name: /organizations/i }, { timeout: 10000 });
    expect(pageTitle).toBeTruthy();
  });

  test('renders the news page', async() => {
    const newsLink = screen.getByRole('link', { name: /news/i });
    expect(newsLink).toBeTruthy();
    fireEvent.click(newsLink);
    const pageTitle = await screen.findByRole('heading', { name: /news/i }, { timeout: 15000 });
    expect(pageTitle).toBeTruthy();
  }, 15000);

  test('renders country instance page with correct data', async () => {
    const countriesLink = screen.getByRole('link', { name: /countries/i });
    expect(countriesLink).toBeTruthy();
    fireEvent.click(countriesLink);
    const sudanLinks = await waitFor(() => screen.getByText(/Afghanistan/i, { selector: '.card-title' }), { timeout: 10000 });
    fireEvent.click(sudanLinks);
    const pageTitle = screen.getByText(/Afghanistan/i, { selector: '.text-center.mb-4' });
    expect(pageTitle).toBeTruthy();
  });

  test('renders organizations instance page with correct data', async () => {
    const organizationsLink = screen.getByRole('link', { name: /organizations/i });
    expect(organizationsLink).toBeTruthy();
    fireEvent.click(organizationsLink);
    await waitFor(() => {
      const unhcrLinks = screen.getAllByText(/Internews/i, { selector: '.card-title' }); 
      const randomIndex = Math.floor(Math.random() * unhcrLinks.length);
      fireEvent.click(unhcrLinks[randomIndex]);
    }, { timeout: 10000 });
    const pageTitle = screen.getByText(/Internews/i, { selector: '.mb-4' });
    expect(pageTitle).toBeTruthy();
  });

  test('renders news instance page', async () => {
    const newsLink = screen.getByRole('link', { name: /news/i });
    expect(newsLink).toBeTruthy();
    fireEvent.click(newsLink);
  }, 15000);

});