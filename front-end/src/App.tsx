import React from 'react';
import './App.css';
import {
  BrowserRouter,
  Routes,
  Route,
  Navigate,
} from 'react-router-dom'
import AboutPage from './components/AboutPage'
import CharitiesPage from './components/CharitiesPage'
import CountriesPage from './components/CountriesPage'
import HomePage from './components/HomePage'
import NewsPage from './components/NewsPage'
import NavBar from './components/NavBar'
import CharityInstance from "./components/CharityInstance"
import CountryInstance from "./components/CountryInstance"
import NewsInstance from "./components/NewsInstance"

 
function App() {

  return (
    <BrowserRouter>
      <NavBar/>
      <Routes>
        <Route path="/" element={<Navigate replace to="/home" />}/>
        <Route path="/home" element={<HomePage />}/>
        <Route path="/about" element={<AboutPage />}/>
        <Route path="/countries" element={<CountriesPage />}/>
        <Route path="/orgs" element={<CharitiesPage />}/>
        <Route path="/news" element={<NewsPage />}/>
        <Route path="/countries/:name" element={<CountryInstance />} />
        <Route path="/news/:id" element={<NewsInstance />} />
        <Route path="/orgs/:name" element={<CharityInstance />} />
      </Routes>
    </BrowserRouter>
  );
}

export default App;
