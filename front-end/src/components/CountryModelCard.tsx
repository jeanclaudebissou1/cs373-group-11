import React from "react";
import { useNavigate } from "react-router-dom";
import { Buffer } from "buffer";

interface CardProps {
  name: string;
  code: string;
  flag_url: string;
  chart: { [key: number]: any };
  refugees: number;
  asylum_seekers: number;
  population: number;
  distance: number;
  news_titles: [number];
  news_id: [number];
  org_names: [string];
  org_short_names: [string];
}

interface CardComponentProps {
  data: CardProps;
}

const CountryModelCard: React.FC<CardComponentProps> = ({ data }) => {
  const navigate = useNavigate();
  return (
    <div className="col-md-4 mb-4" data-testid="country-model-card">
      <div
        className="card p-3 border rounded-3 shadow"
        onClick={() => navigate(`/countries/${data.name}`, {state:{data: data}})}
        style={{ width: "300px", height: "450px" }}
      >
        <img
          src={Buffer.from(data.flag_url, 'utf8').toString()}
          className="card-img-top"
          style={{ height: "200px", objectFit: "cover" }}
          alt={data.flag_url}
        />
        <div className="card-body">
          <h5 className="card-title">{data.name}</h5>
          <p className="card-text">Pop. accepted: {data.refugees}</p>
          <p className="card-text">
            Number of asylum seekers: {data.asylum_seekers}
          </p>
          <p className="card-text">Total pop.: {data.population}</p>
          <p className="card-text">
            Distance from South Sudan: {data.distance}
          </p>
        </div>
      </div>
    </div>
  );
};

export default CountryModelCard;
