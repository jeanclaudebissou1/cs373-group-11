interface Tools {
  name: string;
  image: any;
  desc: string;
  link: string;
}

interface ToolCardProps {
  ToolCardData: Tools;
}

const ToolCard: React.FC<ToolCardProps> = ({ ToolCardData }) => {
  //separate data ToolCardData and pass in something else to CardComponent
  return (
    <div className="col-md-4 mb-4">
      <div
        className="card p-2 mb-2 border rounded-3 shadow"
        style={{ width: "200px", height: "250px" }}
      >
        <a href={ToolCardData.link}>
          <img
            src={require(`../media/${ToolCardData.image}`)}
            className="card-img-top"
            style={{ objectFit: "cover" }}
            alt="Tool Logo"
          />
        </a>
        <div className="card-body">
          <h5 className="card-title text-center">
            <b>{ToolCardData.name}</b>
          </h5>
          {/* <p className="card-text">{ToolCardData.desc}</p> */}
        </div>
      </div>
    </div>
  );
};

export default ToolCard;
