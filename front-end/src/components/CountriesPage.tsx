import React from "react";
import { useState, useEffect } from "react";
import CountryModelCard from "./CountryModelCard";
import Footer from "./Footer";

import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";

import axios from "axios";

interface CountryInstance {
  name: string;
  code: string;
  flag_url: string;
  chart: { [key: number]: any };
  refugees: number;
  asylum_seekers: number;
  population: number;
  distance: number;
  news_titles: [number];
  news_id: [number];
  org_names: [string];
  org_short_names: [string];
}

function CountriesPage() {
  const numCardsPerPage = 6;
  const totalInstances = 125;
  const [currentPage, setCurrentPage] = useState(1);
  const [loaded, setLoaded] = useState(false);
  // const [totalPages, setTotalPages] = useState(0);
  const totalPages = Math.ceil(125 / numCardsPerPage);
  const [countryInstances, setCountryInstances] = useState<CountryInstance[]>([]);

  const nextPage = () => {
    setCurrentPage((prevPage) => Math.min(prevPage + 1, totalPages));
  };

  const prevPage = () => {
    setCurrentPage((prevPage) => Math.max(prevPage - 1, 1));
  };

  const goToPage = (pageNumber: number) => {
    setCurrentPage(pageNumber);
  };


  useEffect(() => {
    axios
      .get(`https://api.supportsouthsudan.me/countries?page=${currentPage}&limit=${numCardsPerPage}`)
      .then((response) => {
        setCountryInstances(response.data.countries);
        // console.log(response.data.countries);
        setLoaded(true);
        // setTotalPages(Math.ceil(250 / numCardsPerPage));
      })
      .catch((error) => {
        console.log("Error", error);
        setLoaded(true); // Make sure to set loaded to true even in case of error
      });
  }, [currentPage]); //NOTE: may have to include countryInstances here, but not doing so leads to less glitching

  if (!loaded) {
    return <h1 style={{ textAlign: "center" }}>Page Loading...</h1>;
  }
  
  return (
    <div> 
      <div className="container mt-5">
        <h1 className="text-center mb-4">Countries</h1>
        <p>
          A comprehensive collection of information about the countries that have
          accepted South Sudanese refugees. It includes details such as the name
          of the country, its geographical distance from Sudan, and population
          demographics. The Countries model serves to contextualize the refugee
          situation by highlighting the global response and providing insights
          into the social, political, and economic environments where South
          Sudanese refugees seek asylum.
        </p>
        <p data-testid="instance-counter">Instances in this model: {totalInstances}</p>
        <p>Number of pages {totalPages}</p>
        <p>Current page: {currentPage}</p>
        <Row
          className="d-flex justify-content-center"
          xs={1}
          md={3}
          lg={3}
          xlg={4}
        >
          {countryInstances && countryInstances.map((instance) => {
            return (
              <Col className="mb-3" key={instance.code}>
                <CountryModelCard data={instance} />
              </Col>
            );
          })}
        </Row>
        <nav aria-label="Page navigation">
          <ul className="pagination">
            <button
              className="btn-dark"
              onClick={prevPage}
              disabled={currentPage === 1}
            >
              Prev
            </button>
            {Array.from({length: totalPages}, (_, i) => i + 1).map(pageNumber => (
              <button
                key={pageNumber}
                style={currentPage === pageNumber ? {backgroundColor: "lightgray"} : {backgroundColor: "white"}}
                onClick={() => goToPage(pageNumber)}
              >
                {pageNumber}
              </button>
            ))}
            <button
              className="btn-dark"
              onClick={nextPage}
              disabled={currentPage === totalPages}
            >
              Next
            </button>
          </ul>
        </nav>
      </div>
      <div>
        <Footer/>
      </div>
    </div>
  );
}

export default CountriesPage;