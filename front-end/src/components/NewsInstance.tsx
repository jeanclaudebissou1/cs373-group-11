// NewsInstancePage.tsx
import React from "react";
// import {useEffect, useState} from "react";
import { GoogleMap, Marker } from "@react-google-maps/api";
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import "./InstancePage.css";
import InstanceCard from"./InstanceCard";
import { Link, useLocation } from "react-router-dom";
import { Button } from "react-bootstrap";

// import axios from "axios"

export interface NewsInstanceData {
  id: number;
  Title: string;
  Date: string;
  Sources: [string];
  Sources_Short:[string];
  Image: string;
  Countries: string[];
  Type: string;
  Text: string;
  Link: string;
  Map: { [key: number]: any };
}

// interface InstancePageProps {
//   data: NewsInstanceData;
// }

const InstancePage = () => {
  const location = useLocation();
  // const [news_info, set_info] = useState(0)
  // useEffect(() => {
  //   axios

  // })
  // console.log(location.state.data);
  const center = {
    lat: location.state.data.Map[0],
    lng: location.state.data.Map[1],
  };
  return (
    <div className="container mt-4">
      <h1 className="text-center mb-4">{location.state.data.Title}</h1>
      <div className="row" style={{ marginTop: "50px" }}>
        <div className="col-md-6">
          <img
            src={location.state.data.Image}
            alt="News"
            className="img-fluid"
          />
          <div className="description-box mb-4">
            <div className="description-content" style={{ marginTop: "30px" }}>
              {location.state.data.Text}
            </div>
          </div>
        </div>
        <div className="col-md-6">
          <div className="data-box">
            <div className="data-row">
              <span className="data-label">Published: </span>
              <span className="data-value">{location.state.data.Date}</span>
            </div>
            <br />
            <div className="data-row">
              <span className="data-label">Type of article: </span>
              <span className="data-value">{location.state.data.Type}</span>
            </div>
            <br />
            <br />
            <div
              className="data-row d-flex justify-content-center"
              style={{ width: "350px", height: "250px" }}
            >
              <GoogleMap
                mapContainerStyle={{
                  width: "100%",
                  height: "100%",
                }}
                zoom={10}
                center={center}
              >
                <Marker position={center} />
              </GoogleMap>
            </div>
          </div>
        </div>
      </div>
      <Button variant="outline-success">
          <Link 
            style={{ color: "black", textDecoration: "inherit"}}
            to={`${location.state.data.Link}`}>
          More Info
          </Link>
        </Button>
      <div className="row" style={{marginTop: '150px'}}>
        <div className="row-md-6">
          <div className="data-row">
            <span className="data-label">Sources: </span>
            <Row>
              {location.state.data.Sources_Short && location.state.data.Sources_Short.map((sourceWord: string, index: number) => {
                const trimmed = sourceWord.trim();
                if (trimmed) {
                  return (
                    <Col key={index} xs={6} md={4} lg={3}>
                      <InstanceCard instanceCardData={{ name:  trimmed, link: `../../orgs/${trimmed}`, inst_type: "orgs", news_id: -1}} />
                      {/* <InstanceCard instanceCardData={{ name:  trimmed}} /> */}
                    </Col>
                  );
                }
                return null;
              })}
            </Row>
          </div>
        </div>
        <div className="row-md-6">
          <div className="data-row">
            <span className="data-label">Countries: </span>
            <Row>
              {location.state.data.Countries && location.state.data.Countries.slice(0, 5).map((country: string, index: number) => {
                const trimmed = country.trim();
                if (trimmed) {
                  return (
                    <Col key={index} xs={6} md={4} lg={3}>
                      <InstanceCard instanceCardData={{ name:  trimmed, link: `../../countries/${trimmed}`, inst_type: "countries", news_id: -1}} />
                    </Col>
                  );
                }
                return null;
              })}
            </Row>
          </div>
        </div>
      </div>
    </div>
  );
};

export default InstancePage;
