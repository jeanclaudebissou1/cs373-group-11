import React from "react";

function Footer() {
  // const navigate = useNavigate();

  return (
    <footer
      className="footer"
      style={{
        position: "sticky",
        width: "100%",
        backgroundColor: "#95dedb",
        color: "black",
      }}
    >
      <div className="container">
        <div className="container-fluid">
          <p className="navbar-brand" style={{ left: 0 }}>
            SupportSouthSudan
          </p>
        </div>
      </div>
    </footer>
  );
}

export default Footer;
