import React from "react";
import { useNavigate } from "react-router-dom";

interface CardProps {
  id: number;
  Title: string;
  Date: string;
  Sources: [string];
  Sources_Short:[string]
  Image: string;
  Countries: [string];
  Type: string;
  Text: string;
  Link: string;
  Map: { [key: number]: any };
}

interface CardComponentProps {
  data: CardProps;
}

const NewsModelCard: React.FC<CardComponentProps> = ({ data }) => {
  const navigate = useNavigate();

  // const countriesToShow = data.Countries.slice(0, 5);
  // const moreCountries = data.Countries.length > 5;
  return (
    <div className="col-md-4 mb-4" data-testid="news-model-card">
      <div
        className="card p-3 border rounded-3 shadow"
        onClick={() => navigate(`/news/${data.id}`, { state: { data: data } })}
        style={{ width: "400px", height: "650px" }}
      >
        <img
          src={data.Image}
          className="card-img-top"
          style={{ height: "200px", objectFit: "cover" }}
          alt={data.Image}
        />
        <div
          className="card-body"
          style={{ maxHeight: "600px", overflowY: "auto" }}
        >
          <h5 className="card-title">{data.Title}</h5>
          <br />
          <p className="card-text">Published: {data.Date}</p>
          <p className="card-text">Sources: {data.Sources_Short}</p>
          {/* <p className="card-text">
            Countries: {countriesToShow.join(", ")} {moreCountries && "..."}
          </p> */}
          <p className="card-text">Type of news: {data.Type}</p>
          <p className="card-text">Countries: 
            {data.Countries.map(countries => {
              return <p>{countries} ,</p>
            })}
          </p>
        </div>
      </div>
    </div>
  );
};

export default NewsModelCard;
