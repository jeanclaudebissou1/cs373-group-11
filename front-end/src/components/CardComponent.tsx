// CardComponent.tsx
import React from 'react';
import { useNavigate } from "react-router-dom"

interface CardProps {
  imageFileName: string;
  title: string;
  content: string;
  model: string;
}

interface CardComponentProps {
  data: CardProps;
}

const CardComponent: React.FC<CardComponentProps> = ({ data }) => {
  const { imageFileName, title, content, model } = data;
  const navigate = useNavigate();
  return (
    <div className="col-md-4 mb-4">
      <div className="card p-3 border rounded-3 shadow" 
      /*CHANGE THIS URL TO BE MORE GENERIC*/
      onClick={() => navigate(`/${model}/${title}`)}
      style={{ width: '300px', height: '400px' }} >
        <img src={require(`../../images/${imageFileName}`)} className="card-img-top" style={{ height: '200px', objectFit: 'cover' }} alt="Card image" />
        <div className="card-body">
          <h5 className="card-title">{title}</h5>
          <p className="card-text">{content}</p>
        </div>
      </div>
    </div>
  );
};

export default CardComponent;
