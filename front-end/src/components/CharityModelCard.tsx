import React from 'react';
import { useNavigate } from "react-router-dom"

interface CardProps {
  name: string;
  image: string;
  date_created: string;
  country: [string];
  type: string;
  description: string;
  homepage: string;
  status: string;
  news_id: [number];
  news: [string];
}

interface CardComponentProps {
  data: CardProps;
}

const CharityModelCard: React.FC<CardComponentProps> = ({ data }) => {
  const navigate = useNavigate();
  return (
    <div className="col-md-4 mb-4" data-testid="charity-model-card">
      <div className="card p-3 border rounded-3 shadow" 
      onClick={() => navigate(`/orgs/${data.name}`, {state:{data: data}})}
      style={{ width: '300px', height: '500px' }} >
        <img src={data.image} className="card-img-top" style={{ height: '200px', objectFit: 'cover' }} alt={data.image}/>
        <div className="card-body" style={{ maxHeight: "600px", overflowY: "auto" }}>
          <h5 className="card-title">{data.name}</h5>
          <p className="card-text">Type of organization: {data.type}</p>
          <p className="card-text">Headquarters: {data.country[0]}</p>
          <p className="card-text">Established: {data.date_created}</p>
          <p className="card-text">Status: {data.status}</p>
        </div>
      </div>
    </div>
  );
};

export default CharityModelCard;
