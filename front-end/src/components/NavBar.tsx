import React from "react";
import { Link } from "react-router-dom";

function NavBar() {
  // const navigate = useNavigate();

  return (
    <nav className="navbar navbar-expand-lg navbar-light" style={{ backgroundColor: "#95dedb", zIndex: 1000, overflow: "auto", whiteSpace: "nowrap", position: "sticky", top: 0, width: "100%"}}>
        <div className="container-fluid" >
            <Link to="/home" className="navbar-brand">SupportSouthSudan</Link>
            <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span className="navbar-toggler-icon"></span>
            </button>
            <div className="collapse navbar-collapse" id="navbarSupportedContent">
                <ul className="navbar-nav me-auto mb-2 mb-lg-0">
                    <li className="nav-item">
                        <Link to="/about" className="nav-link">About</Link>
                    </li>
                    <li className="nav-item">
                        <Link to="/countries" className="nav-link">Countries</Link>
                    </li>
                    <li className="nav-item">
                        <Link to="/orgs" className="nav-link">Organizations</Link>
                    </li>
                    <li className="nav-item">
                        <Link to="/news" className="nav-link">News</Link>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
  );
}

export default NavBar;
