// InstancePage.tsx
import React from 'react';
import {useEffect, useState} from 'react';
import './InstancePage.css';
import YouTube from 'react-youtube';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import "./InstancePage.css";
import InstanceCard from"./InstanceCard";
import { useLocation } from "react-router-dom";
import axios from 'axios';

export interface InstanceData {
  name: string;
  image: string;
  date_created: string;
  // amtRaised: string;
  // numSupporters: number;
  country: [string];
  type: string;
  description: string;
  homepage: string;
  status: string;
  news_id: [number];
  news: [string];
}

// interface InstancePageProps {
//   data: InstanceData;
// }

const CharityInstance = () => {
  const location = useLocation();
  // console.log(location.state.data.imageFileName);
  const [youtubeID, setYoutubeID] = useState("")

  useEffect(() => {
    const params = {
      part: "snippet",
      maxResults: 1,
      q: location.state.data.name, //UPDATE QUERY???
      key: "AIzaSyAbWFmFJQd6aGqDL6Xwwt8N0TZOZLSQVXc",
      type: "video"
    };
    axios
      .get("https://www.googleapis.com/youtube/v3/search", {params})
      .then((response) => {
        const items = response.data.items;
        if (items && items.length > 0) {
          const video_id = items[0].id.videoId;
          setYoutubeID(video_id);
        }
      })
  })
  return (
    <div className="container mt-4">

      <div className="row align-items-center" style={{ marginTop: '50px' }}>
        <div className="col-md-3">
          {/* Logo image */}
          <img src={location.state.data.image} alt="Logo" className="img-fluid" />
        </div>
        <div className="col-md-9">
          {/* Title */}
          <h1 className="mb-4" style={{ fontSize: '50px' }}>{location.state.data.name}</h1>

        </div>
      </div>
      <div className="row">
        <div className="col-md-6">
          <div className="description-box mb-4" style={{ marginTop: '50px' }}>
            <p>{location.state.data.description}</p>
            {/* <p>{mission}</p>
            <p>{projects}</p> */}
            <YouTube videoId={youtubeID} style={{marginTop: '50px'}}/>
          </div>
        </div>
        <div className="col-md-6">
          <div className="data-box" style={{ marginTop: '50px' }}>
            <div className="data-row">
              <span className="data-label">Status: </span>
              <span className="data-value">{location.state.data.status}</span>
            </div>
            <div className="data-row">
              <span className="data-label">Headquarters Location: </span>
              <span className="data-value">{location.state.data.country}</span>
            </div>
            <div className="data-row">
              <span className="data-label">Date of Establishment: </span>
              <span className="data-value">{location.state.data.date_created}</span>
            </div>
            <div className="data-row">
              <span className="data-label">Type of organization: </span>
              <span className="data-value">{location.state.data.type}</span>
            </div>
            <br />
            <div className="data-row">
              <button
                onClick={() => window.open(location.state.data.Link, "_blank")}
                className="btn btn-dark"
              >
                Homepage
              </button>
            </div>
            <br /> 
          </div>
        </div>
        <div className="row" style={{marginTop: '150px'}}>
        <div className="row-md-6">
          <div className="data-row">
            <span className="data-label">News: </span>
            <Row>
              {location.state.data.news && location.state.data.news.slice(0, 5).map((news: string, index: number) => {
                const trimmed = news.trim();
                if (trimmed) {
                  return (
                    <Col key={index} xs={6} md={4} lg={3}>
                      <InstanceCard instanceCardData={{ name:  trimmed, link: `../../news/${trimmed}`, inst_type: "news", news_id: location.state.data.news_id[index]}} />
                    </Col>
                  );
                }
                return null;
              })}
            </Row>
          </div>
        </div>
        <div className="row-md-6">
          <div className="data-row">
            <span className="data-label">Countries: </span>
            <Row>
              {location.state.data.country && location.state.data.country.slice(0, 5).map((country: string, index: number) => {
                const trimmed = country.trim();
                if (trimmed) {
                  return (
                    <Col key={index} xs={6} md={4} lg={3}>
                      <InstanceCard instanceCardData={{ name:  trimmed, link: `../../countries/${trimmed}`, inst_type: "countries", news_id: -1}} />
                    </Col>
                  );
                }
                return null;
              })}
            </Row>
          </div>
        </div>
      </div>
      </div>
    </div>
  );
};

export default CharityInstance;
