import React from "react";
import { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";

import axios from "axios";

interface InstanceData {
  name: string;
  link: string;
  inst_type: string;
  news_id: any;
}

// Define the props interface for the component
interface InstanceCardProps {
  instanceCardData: InstanceData; // Change to instanceCardData
}

//Component definition
const InstanceCard: React.FC<InstanceCardProps> = ({ instanceCardData }) => {
  const [loaded, setLoaded] = useState(false);
  const navigate = useNavigate()
  const [instanceData, setInstanceData] = useState();
  var data_name = instanceCardData.name
  if (instanceCardData.inst_type === "news"){
    data_name = instanceCardData.news_id
  }
    useEffect(() => {
      axios
        .get(`http://127.0.0.1:9000/${instanceCardData.inst_type}/${data_name}`)
        .then((response) => {
          console.log("http://127.0.0.1:9000/"+ instanceCardData.inst_type +"/" +data_name)
          if (instanceCardData.inst_type === "news"){
            setInstanceData(response.data);
          } else if (instanceCardData.inst_type === "orgs"){
            setInstanceData(response.data);
          } else{
            setInstanceData(response.data);
          }
          setLoaded(true);
      })
      .catch((error) => {
        console.log("Error", error);
        setLoaded(true);
      })
    }, [data_name, instanceCardData.inst_type])
    console.log(instanceData)

    if (!loaded) {
      return <h1 style={{ textAlign: "center" }}>Card Loading...</h1>;
    }

  return (
    <div className="col-md-4 mb-4">
        <div
          className="card p-3 border rounded-3 shadow"
          onClick={() => navigate(`/${instanceCardData.inst_type}/${data_name}`, {state:{data: instanceData}})}
          style={{ width: "300px", height: "125px" }}
        >
          <div className="card-body" style={{overflow:"hidden"}}>
            <h5 className="card-title text-center">
              <b>{instanceCardData.name}</b>
            </h5>
          </div>
        </div>
    </div>
  );
};

// const InstanceCard: React.FC<InstanceCardProps> = ({ instanceCardData }) => {
//   return (
//     <div className="col-md-4 mb-4">
//       <a
//         href={instanceCardData.link}
//         className="card-link"
//         style={{ textDecoration: "none", color: "inherit" }}
//       >
//         <div
//           className="card p-3 border rounded-3 shadow"
//           style={{ width: "300px", height: "125px" }}
//         >
//           <div className="card-body" style={{overflow:"hidden"}}>
//             <h5 className="card-title text-center">
//               <b>{instanceCardData.name}</b>
//             </h5>
//           </div>
//         </div>
//       </a>
//     </div>
//   );
// };


export default InstanceCard;
