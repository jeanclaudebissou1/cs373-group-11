import { Member } from "../gitlab_api";

interface AboutCardProps {
  AboutCardData: Member;
}

const AboutCard: React.FC<AboutCardProps> = ({ AboutCardData }) => {
  //separate data AboutCardData and pass in something else to CardComponent
  return (
    <div className="col-md-4 mb-4">
      <div
        className="card p-3 mb-3 border rounded-3 shadow"
        style={{ width: "300px", height: "625px" }}
      >
        <img
          src={require(`../media/${AboutCardData.photo}`)}
          className="card-img-top"
          style={{ height: "320px", objectFit: "cover" }}
          alt={AboutCardData.name}
        />
        <div className="card-body">
          <h5 className="card-title">
            <b>{AboutCardData.name}</b>
          </h5>
          <h6 className="card-title">
            <b>Role:</b> {AboutCardData.role}
          </h6>
          <h6 className="card-title">
            <b>Major:</b> {AboutCardData.major}
          </h6>
          <h6 className="card-title">
            <b>Commits:</b> {Math.floor(AboutCardData.commits / 2)}
          </h6>
          <h6 className="card-title">
            <b>Issues:</b> {AboutCardData.issues}
          </h6>
          <h6 className="card-title">
            <b>Tests:</b> {AboutCardData.tests}
          </h6>
          <p className="card-text">{AboutCardData.bio}</p>
        </div>
      </div>
    </div>
  );
};

export default AboutCard;
