import React from "react";
import { useState, useEffect } from "react";
import CharityModelCard from "./CharityModelCard";
import Footer from "./Footer";

import Row from "react-bootstrap/Row"
import Col from "react-bootstrap/Col"

import axios from "axios";

interface CharitiesInstance {
  name: string;
  image: string;
  date_created: string;
  // amtRaised: string;
  // numSupporters: number;
  country: [string];
  type: string;
  description: string;
  homepage: string;
  status: string;
  news_id: [number];
  news: [string];
}

function CharitiesPage() {
  const numCardsPerPage = 6;
  const totalInstances = 84;
  const [currentPage, setCurrentPage] = useState(1);
  const [loaded, setLoaded] = useState(false);
  // const [totalPages, setTotalPages] = useState(0);
  const totalPages = Math.ceil (84 / numCardsPerPage)
  const [charitiesInstances, setCharitiesInstances] = useState<CharitiesInstance[]>([]);

  const nextPage = () => {
    setCurrentPage((prevPage) => Math.min(prevPage + 1, totalPages));
  };

  const prevPage = () => {
    setCurrentPage((prevPage) => Math.max(prevPage - 1, 1));
  };

  const goToPage = (pageNumber: number) => {
    setCurrentPage(pageNumber);
  };

  useEffect(() => {
    axios
      .get(`https://api.supportsouthsudan.me/orgs?page=${currentPage}&limit=${numCardsPerPage}`)
      .then((response) => {
        // console.log(response.data.orgs);
        setCharitiesInstances(response.data.orgs);
        setLoaded(true);
        // setTotalPages(Math.ceil(response.data.total / numCardsPerPage));
      })
      .catch((error) => {
        console.log("Error", error);
        setLoaded(true);
      });
  }, [currentPage])

  if (!loaded) {
    return <h1 style={{ textAlign: "center" }}>Page Loading...</h1>;
  }

  return (
    <div>
      <div className="container mt-5">
        <h1 className="text-center mb-4">Organizations</h1>
        <p>
          A catalog of various charities and organizations actively involved in
          providing assistance and support to South Sudanese refugees. It
          includes information about their mission, types of assistance offered,
          whether they operate as volunteer or donation-based enterprises, as
          well as details on their location and year of establishment. This
          model showcases the diverse range of efforts undertaken by charitable
          organizations to address the needs of refugees. It serves as a
          resource for individuals and groups seeking opportunities to
          contribute, volunteer, or collaborate with organizations dedicated to
          refugee assistance.
        </p>
        <p data-testid="instance-counter">Instances in this model: {totalInstances}</p>
        <p>Number of pages {totalPages}</p>
        <p>Current page: {currentPage}</p>
        <Row
          className="d-flex justify-content-center"
          xs={1}
          md={3}
          lg={3}
          xlg={4}
        >
          {charitiesInstances.map((instance) => {
            return(
              <Col className="mb-3" key={instance.name}>
                <CharityModelCard data={instance}/>
              </Col>
              
            );
          })}
        </Row>
        <nav aria-label="Page navigation">
          <ul className="pagination">
            <button
              className="btn-dark"
              onClick={prevPage}
              disabled={currentPage === 1}
            >
              Prev
            </button>
            {Array.from({ length: totalPages }, (_, i) => i + 1).map(
              (pageNumber) => (
                <button
                  key={pageNumber}
                  style={currentPage === pageNumber ? {backgroundColor: "lightgray"} : {backgroundColor: "white"}}
                  onClick={() => goToPage(pageNumber)}
                >
                  {pageNumber}
                </button>
              )
            )}
            <button
              className="btn-dark"
              onClick={nextPage}
              disabled={currentPage >= totalPages}
            >
              Next
            </button>
          </ul>
        </nav>
      </div>
      <div>
        <Footer />
      </div>
    </div>
  );
}

export default CharitiesPage;
