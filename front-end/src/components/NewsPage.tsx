import { useState, useEffect } from "react";
import NewsModelCard from "./NewsModelCard";
import Footer from "./Footer";

import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";

import axios from "axios";

interface NewsInstance {
  id: number;
  Title: string;
  Date: string;
  Sources: [string];
  Sources_Short:[string];
  Image: string;
  Countries: [string];
  Type: string;
  Text: string;
  Link: string;
  Map: { [key: number]: any };
}

function NewsPage() {
  const numCardsPerPage = 6;
  const totalInstances = 200;
  const [currentPage, setCurrentPage] = useState(1);
  const [loaded, setLoaded] = useState(false);
  // const totalPages = Math.ceil(250 / numCardsPerPage);
  const totalPages = Math.ceil(totalInstances / numCardsPerPage);
  const [newsInstances, setNewsInstances] = useState<NewsInstance[]>([]);

  const nextPage = () => {
    setCurrentPage((prevPage) => Math.min(prevPage + 1, totalPages));
  };

  const prevPage = () => {
    setCurrentPage((prevPage) => Math.max(prevPage - 1, 1));
  };

  const goToPage = (pageNumber: number) => {
    setCurrentPage(pageNumber);
  };

  useEffect(() => {
    axios
      .get(
        `https://api.supportsouthsudan.me/news?page=${currentPage}&limit=${numCardsPerPage}`
      )
      .then((response) => {
        setNewsInstances(response.data.news);
        setLoaded(true);
        // setTotalPages(Math.ceil(response.data.total / numCardsPerPage));
      })
      .catch((error) => {
        console.log("Error", error);
        setLoaded(true); // Make sure to set loaded to true even in case of error
      });
  }, [currentPage]);

  if (!loaded) {
    return <h1 style={{ textAlign: "center" }}>Page Loading...</h1>;
  }
  // console.log(newsInstances);
  return (
    <div>
      <div className="container mt-5">
        <h1 className="text-center mb-4">News</h1>
        <p>
          An aggregated and organized collection of relevant news articles and
          updates related to South Sudanese refugees. It encompasses a variety
          of sources, including mainstream media, humanitarian organizations,
          governmental agencies, and independent journalism, to offer a
          comprehensive overview of current events and developments affecting
          the refugee community. This model aims to keep stakeholders informed
          about the latest developments, challenges, and progress in addressing
          the refugee crisis, fostering empathy, and promoting advocacy efforts.
        </p>
        <p>Instances in this model: {totalInstances}</p>
        <p>Number of pages {totalPages}</p>
        <p>Current page: {currentPage}</p>
        <Row
          className="d-flex justify-content-center"
          xs={1}
          md={3}
          lg={3}
          xlg={4}
        >
          {newsInstances &&
            newsInstances.map((instance) => {
              return (
                <Col className="mb-3" key={instance.id}>
                  <NewsModelCard data={instance} />
                </Col>
              );
            })}
        </Row>
        <nav aria-label="Page navigation">
          <ul className="pagination">
            <button
              className="btn-dark"
              onClick={prevPage}
              disabled={currentPage === 1}
            >
              Prev
            </button>
            {Array.from({ length: totalPages }, (_, i) => i + 1).map(
              (pageNumber) => (
                <button
                  key={pageNumber}
                  style={
                    currentPage === pageNumber
                      ? { backgroundColor: "lightgray" }
                      : { backgroundColor: "white" }
                  }
                  onClick={() => goToPage(pageNumber)}
                >
                  {pageNumber}
                </button>
              )
            )}
            <button
              className="btn-dark"
              onClick={nextPage}
              disabled={currentPage === totalPages}
            >
              Next
            </button>
          </ul>
        </nav>
      </div>
      <div>
        <Footer />
      </div>
    </div>
  );
}

export default NewsPage;
