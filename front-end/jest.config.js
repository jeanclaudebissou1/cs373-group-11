module.exports = {
    preset: 'ts-jest',
    testEnvironment: 'jsdom',
    moduleNameMapper: {
      "\\.(css|less|sass|scss)$": "identity-obj-proxy",
      "\\.(jpg|jpeg|png|gif|eot|otf|webp|svg|ttf|woff|woff2|mp4|webm|wav|mp3|m4a|aac|oga)$": "<rootDir>/__mocks__/fileMock.js"
    },
    roots: ["<rootDir>/src", "<rootDir>/__tests__"],
    moduleFileExtensions: ["js", "jsx", "ts", "tsx", "json", "node"],
    testMatch: ["**/__tests__/**/*.test.{ts,tsx,js,jsx}"],
    collectCoverageFrom: ["src/**/*.{ts,tsx,js,jsx}", "!<rootDir>/node_modules/"],
    coverageReporters: ["json", "lcov", "text", "html"],
    // setupFilesAfterEnv: ["<rootDir>/setupTests.ts"],
  };  