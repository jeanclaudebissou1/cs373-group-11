BRANCH := develop

install:
	npm install

start:
	npm run start

build:
	npm run build

pull:
	git pull origin $(BRANCH)

