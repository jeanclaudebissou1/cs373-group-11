from operator import concat
import os
import subprocess
import ssl
import certifi
import base64
import requests
import time

from models import countries_news, news_orgs, Countries1, Orgs, News

from flask import *
from flask_cors import CORS
from sqlalchemy import Column, Integer, String, Text, create_engine, select, Table, MetaData, text, case, func, Float, ForeignKey
from sqlalchemy.orm import sessionmaker, aliased
from dotenv import load_dotenv
from requests.exceptions import HTTPError
import json

load_dotenv()

dialect = "mysql"
driver = "pymysql"
username = os.getenv('DB_USERNAME')
password = os.getenv('DB_PASSWORD')
host = os.getenv('DB_HOST')
port = 3306
database = os.getenv('DB_NAME')

ssl_context = {
    "ssl": "VERIFY_IDENTITY",
    "ssl_ca": certifi.where(),
}

connection_url = f"{dialect}+{driver}://{username}:{password}@{host}:{port}/{database}"
engine = create_engine(connection_url, connect_args=ssl_context)
Session = sessionmaker(bind=engine)
metadata = MetaData()

GOOGLE_MAPS_API_KEY = 'AIzaSyAhA375sjTU9u3bFuxcY68_r3mge6VaVHg'

Countries = Table(
    'Countries', metadata,
    Column('id', Integer, primary_key=True, autoincrement=True),
    Column('name', String(255), nullable=False),
    Column('code', String(3), unique=True, primary_key=True),
    Column('population', Integer),
    Column('refugees', Integer),
    Column('asylum_seekers', Integer),
    Column('distance', Float),
    Column('flag_url', String(255)),
    Column('chart', String(2000)),
    autoload_with=engine
)

# News_Old = Table('News', metadata, autoload_with=engine)

Organizations = Table(
    'Orgs', metadata,
    Column('id', Integer, primary_key=True, unique=True, autoincrement=False),
    Column('name', String(255)),
    Column('short_name', String(255), index=True),
    Column('image', String(255)),
    Column('homepage', String(255)),
    Column('date_created', String(255)),
    Column('status', String(255)),
    Column('type', String(255)),
    Column('description', Text),
    Column('country_code', String(3), ForeignKey("Countries1.code")),
    autoload_with=engine
)

News_Old = Table(
    'News', metadata,
    Column('id', Integer, primary_key=True),
    Column('title', String(255)),
    Column('date', String(255)),
    Column('image', String(255)),
    Column('text', Text),
    Column('link', String(255)),
    Column('map', Text),
    Column('type', String(255)),
    autoload_with=engine,
    extend_existing=True  # Add this line to allow redefinition
)

app = Flask(__name__)
CORS(app)

model = []

countries = [
    {
        'name' : 'Sudan',
        'accepted_refugees' : 0,
        'num_asylum_seekers' : 0,
        'population' : 0,
        'distance' : 0
    },
    {
        'name' : 'Ethiopia',
        'accepted_refugees' : 0,
        'num_asylum_seekers' : 0,
        'population' : 0,
        'distance' : 0
    },
    {
        'name' : 'Uganda',
        'accepted_refugees' : 0,
        'num_asylum_seekers' : 0,
        'population' : 0,
        'distance' : 0
    },
]

charities = [
    {
        'name' : 'UNHCR',
        'location' : '',
        'est_date' : '',
        'type' : '',
        'num_supporters' : 0,
        'amt_raised' : '$0'
    },
    {
        'name' : 'UNHCR',
        'location' : '',
        'est_date' : '',
        'type' : '',
        'num_supporters' : 0,
        'amt_raised' : '$0'
    },
    {
        'name' : 'UNHCR',
        'location' : '',
        'est_date' : '',
        'type' : '',
        'num_supporters' : 0,
        'amt_raised' : '$0'
    },
]

news = [
    {
        'name' : 'UNHCR',
        'id' : 0,
        'date' : '',
        'source' : '',
        'topic' : '',
        'origin_country' : 0,
        'article_type' : ''
    },
    {
        'name' : 'UNHCR',
        'id' : 0,
        'location' : '',
        'est_date' : '',
        'type' : '',
        'num_supporters' : 0,
        'amt_raised' : ''
    }
]

"""
Countries - GET Endpoints
"""
# Route for getting ALL countries
@app.route('/countries')
def get_countries():
    if request.args.get("page") and request.args.get("limit"):
        page = int(request.args.get("page"))
        limit = int(request.args.get("limit"))
        stmt = (
            select(
                Countries1.id,
                Countries1.name,
                Countries1.code,
                Countries1.population,
                Countries1.refugees,
                Countries1.asylum_seekers,
                Countries1.distance,
                Countries1.flag_url,
                Countries1.chart,
                func.group_concat(News.title).label('news_titles'),
                func.group_concat(News.id).label('news_id'),
                func.group_concat(Orgs.name).label('org_names'),
                func.group_concat(Orgs.short_name).label('org_short_names')
            )
            .join(countries_news, Countries1.code == countries_news.c.country_code)
            .join(News, countries_news.c.news_id == News.id)
            .outerjoin(news_orgs, News.id == news_orgs.c.news_id)
            .outerjoin(Orgs, news_orgs.c.org_name == Orgs.short_name)
            .group_by(
                Countries1.id,
                Countries1.name,
                Countries1.code,
                Countries1.population,
                Countries1.refugees,
                Countries1.asylum_seekers,
                Countries1.distance,
                Countries1.flag_url,
                Countries1.chart
            )
            .offset((page - 1) * limit)
            .limit(limit)
        )
    else:
        stmt = (
            select(
                Countries1.id,
                Countries1.name,
                Countries1.code,
                Countries1.population,
                Countries1.refugees,
                Countries1.asylum_seekers,
                Countries1.distance,
                Countries1.flag_url,
                Countries1.chart,
                func.group_concat(News.title).label('news_titles'),
                func.group_concat(News.id).label('news_id'),
                func.group_concat(Orgs.name).label('org_names'),
                func.group_concat(Orgs.short_name).label('org_short_names')
            )
            .join(countries_news, Countries1.code == countries_news.c.country_code)
            .join(News, countries_news.c.news_id == News.id)
            .outerjoin(news_orgs, News.id == news_orgs.c.news_id)
            .outerjoin(Orgs, news_orgs.c.org_name == Orgs.short_name)
            .group_by(
                Countries1.id,
                Countries1.name,
                Countries1.code,
                Countries1.population,
                Countries1.refugees,
                Countries1.asylum_seekers,
                Countries1.distance,
                Countries1.flag_url,
                Countries1.chart
            )
        )
        
    with engine.connect() as connection:
        connection.execute(text("SET SESSION sort_buffer_size = 3145728;"))
        result = connection.execute(stmt)
        all_rows = result.fetchall()

    # print(all_rows)
    json_data = []
    for row in all_rows:
    # for i in range(0,1):
    #     row = all_rows[0]
    #     print(row)
        row_dict = {
            'name': row[1],
            'code': row[2],
            'population': row[3],
            'refugees': row[4],
            'asylum_seekers': row[5],
            'distance': row[6],
            'flag_url': row[7],
            'chart': row[8],
            'news_titles': row[9].split('*,') if row[9] else [],
            'news_id': row[10].split(',') if row[10] else [],
            'org_names': row[11].split(',') if row[11] else [],
            'org_short_names': row[12].split(',') if row[12] else []
        }
        try:
            if row[7]:
                row_dict['chart'] = json.loads(row[7])
        except json.JSONDecodeError as e:  # Correct the exception name
            print(f"Error decoding JSON: {e}")
        json_data.append(row_dict)
            
    return jsonify({"countries" : json_data}) 

# Route for getting SINGLE country
@app.route('/countries/<string:name>')
def get_country(name):
    stmt = (
        select(
            Countries1.id,
            Countries1.name,
            Countries1.code,
            Countries1.population,
            Countries1.refugees,
            Countries1.asylum_seekers,
            Countries1.distance,
            Countries1.flag_url,
            Countries1.chart,
            func.group_concat(News.title).label('news_titles'),
            func.group_concat(News.id).label('news_id'),
            func.group_concat(Orgs.name).label('org_names'),
            func.group_concat(Orgs.short_name).label('org_short_names')
        )
        .join(countries_news, Countries1.code == countries_news.c.country_code)
        .join(News, countries_news.c.news_id == News.id)
        .outerjoin(news_orgs, News.id == news_orgs.c.news_id)
        .outerjoin(Orgs, news_orgs.c.org_name == Orgs.short_name)
        .group_by(
            Countries1.id,
            Countries1.name,
            Countries1.code,
            Countries1.population,
            Countries1.refugees,
            Countries1.asylum_seekers,
            Countries1.distance,
            Countries1.flag_url,
            Countries1.chart
        )
        .where(Countries1.name == name)
        .limit(1)
    )

    with engine.connect() as connection:
        connection.execute(text("SET SESSION sort_buffer_size = 3145728;"))
        result = connection.execute(stmt)
        country = result.fetchone()

    if country:
        country_data = {
            'name': country[1],
            'code': country[2],
            'population': country[3],
            'refugees': country[4],
            'asylum_seekers': country[5],
            'distance': country[6],
            'flag_url': country[7],
            'chart': country[8],
            'news_titles': country[9].split('*,') if country[9] else [],
            'news_id': country[10].split(',') if country[10] else [],
            'org_names': country[11].split(',') if country[11] else [],
            'org_short_names': country[12].split(',') if country[12] else []
        }
        try:
            if country[7]:
                country_data['chart'] = json.loads(country[7])
        except json.JSONDecodeError as e:
            print(f"Error decoding JSON: {e}")
        return jsonify(country_data)

    return jsonify({'error': 'Country not found'})


"""
Charities - GET Endpoints
"""
# Route for getting ALL organizations
@app.route('/orgs')
def get_charities():
    if(request.args.get("page")):
        page = int(request.args.get("page"))
    else:
        page = 1
    if(request.args.get("limit")):
        limit = int(request.args.get("limit"))
    else:
        limit = 10

    if page:
        if limit:
            stmt = (
                select(
                    Countries1.name,
                    Orgs,
                    func.group_concat(concat(News.title, '*')).label('news_titles'),
                    func.group_concat(News.id).label('news_ids')
                )
                .join(Orgs, Countries1.code == Orgs.country_code)
                .outerjoin(news_orgs, Orgs.short_name == news_orgs.c.org_name)
                .outerjoin(News, news_orgs.c.news_id == News.id)
                .group_by(Countries1.name, Orgs)
                .offset((page - 1) * limit)
                .limit(limit)
            )
        else:
            stmt = (
                select(
                    Countries1.name,
                    Orgs,
                    func.group_concat(concat(News.title, '*')).label('news_titles'),
                    func.group_concat(News.id).label('news_ids')
                )
                .join(Orgs, Countries1.code == Orgs.country_code)
                .outerjoin(news_orgs, Orgs.short_name == news_orgs.c.org_name)
                .outerjoin(News, news_orgs.c.news_id == News.id)
                .group_by(Countries1.name, Orgs)
                .offset((page - 1) * limit)
            )
    else:
        if limit:
            stmt = (
                select(
                    Countries1.name,
                    Orgs,
                    func.group_concat(concat(News.title, '*')).label('news_titles'),
                    func.group_concat(News.id).label('news_ids')
                )
                .join(Orgs, Countries1.code == Orgs.country_code)
                .outerjoin(news_orgs, Orgs.short_name == news_orgs.c.org_name)
                .outerjoin(News, news_orgs.c.news_id == News.id)
                .group_by(Countries1.name, Orgs)
                .limit(limit)
            )
        else:
            stmt = (
                select(
                    Countries1.name,
                    Orgs,
                    func.group_concat(concat(News.title, '*')).label('news_titles'),
                    func.group_concat(News.id).label('news_ids')
                )
                .join(Orgs, Countries1.code == Orgs.country_code)
                .outerjoin(news_orgs, Orgs.short_name == news_orgs.c.org_name)
                .outerjoin(News, news_orgs.c.news_id == News.id)
                .group_by(Countries1.name, Orgs)
            )

    with engine.connect() as conn:
        conn.execute(text("SET SESSION sort_buffer_size = 3145728;"))
        result = conn.execute(stmt)
        orgs = result.fetchall()

    json_data = []
    for org in orgs:
        org_data = {
            "id": org[1],
            "name": org[2],
            "short_name": org[3],
            "image": org[4],
            "homepage": org[5],
            "date_created": org[6],
            "status": org[7],
            "type": org[8],
            "description": org[9],
            "country_code": org[10],
            "news": org[11].split('*,') if org[11] else [],
            "news_id": org[12].split('*,') if org[12] else [],
            "country": [org[0]],
        }
        json_data.append(org_data)

    return jsonify({"orgs": json_data})

# Route for getting SINGLE organization
@app.route('/orgs/<string:name>')
def get_org(name):
    stmt = (
        select(
            Countries1.name,
            Orgs,
            func.group_concat(concat(News.title, '*')).label('news_titles'),
            func.group_concat(News.id).label('news_ids')
        )
        .join(Orgs, Countries1.code == Orgs.country_code)
        .outerjoin(news_orgs, Orgs.short_name == news_orgs.c.org_name)
        .outerjoin(News, news_orgs.c.news_id == News.id)
        .where(Orgs.name == name)
        .group_by(Countries1.name, Orgs)
    )

    with engine.connect() as conn:
        result = conn.execute(stmt)
        orgs = result.fetchall()
        
    print(orgs)
    for org in orgs:   
        if org[2] == name:
            org_data = {
                "id": org[1],
                "name": org[2],
                "short_name": org[3],
                "image": org[4],
                "homepage": org[5],
                "date_created": org[6],
                "status": org[7],
                "type": org[8],
                "description": org[9],
                "country_code": org[10],
                "news": org[11].split('*,') if org[11] else [],
                "news_id": org[12].split('*,') if org[12] else [],
                "country": [org[0]],
            }
            return json.dumps(org_data)
    
    return json.dumps({'error': 'Org not found'})

"""
News - GET Endpoints
"""
# Route for getting all news
@app.route('/news')
def get_news():
    if(request.args.get("page")):
        page = int(request.args.get("page"))
    else:
        page = 1
    if(request.args.get("limit")):
        limit = int(request.args.get("limit"))
    else:
        limit = 200

    if page:
        if limit:
            stmt = (
                select(func.group_concat(concat(Countries1.name,"")).label('country_names'), func.group_concat(concat(Countries1.code,"")).label('country_codes'), Orgs.name, Orgs.short_name, News)
                .join(countries_news, News.id == countries_news.c.news_id)
                .join(Countries1, countries_news.c.country_code == Countries1.code)
                .outerjoin(news_orgs, News.id == news_orgs.c.news_id)
                .outerjoin(Orgs, news_orgs.c.org_name == Orgs.short_name)
                .group_by(News, Orgs.name, Orgs.short_name)
                .offset((page - 1) * limit)
                .limit(limit)
            )
        else:
            stmt = (
                select(func.group_concat(concat(Countries1.name, '')).label('country_names'), func.group_concat(concat(Countries1.code, '')).label('country_codes'), Orgs.name, Orgs.short_name, News)
                .join(countries_news, News.id == countries_news.c.news_id)
                .join(Countries1, countries_news.c.country_code == Countries1.code)
                .outerjoin(news_orgs, News.id == news_orgs.c.news_id)
                .outerjoin(Orgs, news_orgs.c.org_name == Orgs.short_name)
                .group_by(News, Orgs.name, Orgs.short_name)
                .offset((page - 1) * limit)
            )
    else:
        if limit:
            stmt = (
                select(func.group_concat(concat(Countries1.name, '')).label('country_names'), func.group_concat(concat(Countries1.code, '')).label('country_codes'), Orgs.name, Orgs.short_name, News)
                .join(countries_news, News.id == countries_news.c.news_id)
                .join(Countries1, countries_news.c.country_code == Countries1.code)
                .outerjoin(news_orgs, News.id == news_orgs.c.news_id)
                .outerjoin(Orgs, news_orgs.c.org_name == Orgs.short_name)
                .group_by(News)
                .limit(limit)
            )
        else:
            stmt = (
                select(func.group_concat(concat(Countries1.name, '')).label('country_names'), func.group_concat(concat(Countries1.code, '')).label('country_codes'), Orgs.name, Orgs.short_name, News)
                .join(countries_news, News.id == countries_news.c.news_id)
                .join(Countries1, countries_news.c.country_code == Countries1.code)
                .outerjoin(news_orgs, News.id == news_orgs.c.news_id)
                .outerjoin(Orgs, news_orgs.c.org_name == Orgs.short_name)
                .group_by(News, Orgs.name, Orgs.short_name)
            )

    with engine.connect() as conn:
        conn.execute(text("SET SESSION sort_buffer_size = 3145728;"))
        result = conn.execute(stmt)
        news = result.fetchall()

    #print(news)
    json_data  = []
    news_dict = {}
    
    for n in news:
        # country_name = n[0]
        # country_code = n[1]
        news_title = n[5]
    
        if news_title not in news_dict:
            news_dict[news_title] = {
                'id': n[4],
                'Title': n[5],
                'Date': n[6],
                'Sources': [n[3]],
                'Sources_Short': [n[2]],  
                'Image': n[7],
                'Countries': n[0].split(',') if news[0] else [],
                'Countries_Code': n[1].split(',') if news[0] else [],
                'Type': n[8],
                'Text': n[9],
                'Link': n[10],
                'Map': json.loads(n[11])
            }
        # else:
        #     if len(news_dict[news_title]['Countries']) <= 5:
        #         news_dict[news_title]['Countries'].append(country_name)
        #     if len(news_dict[news_title]['Countries_Code']) <= 5:
        #         news_dict[news_title]['Countries_Code'].append(country_code)
        # if  n[2] is not None:
        json_data = list(news_dict.values())
    return jsonify({"news": json_data})

# def get_news():
#     if request.args.get("page"):
#         page = int(request.args.get("page"))
#     else:
#         page = 1

#     if request.args.get("limit"):
#         limit = int(request.args.get("limit"))
#     else:
#         limit = 200

#     stmt = (
#         select(News_Old.c.id, News_Old.c.title, News_Old.c.date,
#                Organizations.c.short_name.label('sources_short'),
#                Organizations.c.name.label('sources'), News_Old.c.image,
#                News_Old.c.text, News_Old.c.link, News_Old.c.map,
#                Countries.c.name.label('countries'),
#                Countries.c.code.label('countries_code'),
#                News_Old.c.type)
#         .select_from(News_Old.join(news_orgs, News_Old.c.id == news_orgs.c.news_id)
#                      .join(Organizations, news_orgs.c.org_name == Organizations.c.short_name)
#                      .join(countries_news, News_Old.c.id == countries_news.c.news_id)
#                      .join(Countries, countries_news.c.country_code == Countries.c.code))
#         .group_by(News_Old.c.id, News_Old.c.title, News_Old.c.date,
#                   Organizations.c.short_name, Organizations.c.name,
#                   News_Old.c.image, News_Old.c.text, News_Old.c.link,
#                   News_Old.c.map, Countries.c.name, Countries.c.code,
#                   News_Old.c.type)
#         .order_by(Countries.c.name, News_Old.c.date.desc())  # Order by country name first, then by date
#         .offset((page - 1) * limit)
#         .limit(limit)
#     )

#     with engine.connect() as conn:
#         conn.execute(text("SET SESSION sort_buffer_size = 3145728;"))
#         result = conn.execute(stmt)
#         news = result.fetchall()

#     country_news = {}  # Dictionary to hold news articles organized by country
#     json_data  = []  # List to hold formatted news data

#     # Function to search for a row by news title
#     def search_by_title(news_title):
#         for row in json_data:
#             if row['Title'] == news_title:
#                 return row
#         return None

#     for n in news:
#         country_name = n[9]
#         news_title = n[1]

#         if news_title not in country_news:
#             country_news[news_title] = [country_name]
#         else:
#             country_news[news_title].append(country_name)

#         row_dict = {
#             'id': n[0],
#             'Title': n[1],
#             'Date': n[2],
#             'Sources_Short': n[3],
#             'Sources': n[4],
#             'Image': n[5],
#             'Text': n[6],
#             'Link': n[7],
#             'Map': json.loads(n[8]),
#             'Countries': country_news[news_title],
#             'Countries_Code': n[10],
#             'Type': n[11]
#         }
#         json_data.append(row_dict)
#         # print(json_data[0])

#         # Update the 'Countries' field in the existing row with news title if it exists
#         existing_row = search_by_title(news_title)
#         if existing_row:
#             existing_row[9] = country_news[news_title]


#     return jsonify({"news": json_data})

# Route for getting single news instance
@app.route('/news/<int:id>')
def get_article(id):
    stmt = (
            select(func.group_concat(concat(Countries1.name, '')).label('country_names'), func.group_concat(concat(Countries1.code, '')).label('country_codes'), Orgs.name, Orgs.short_name, News)
            .join(countries_news, News.id == countries_news.c.news_id)
            .join(Countries1, countries_news.c.country_code == Countries1.code)
            .outerjoin(news_orgs, News.id == news_orgs.c.news_id)
            .outerjoin(Orgs, news_orgs.c.org_name == Orgs.short_name)
            .group_by(News)
        ) 
    
    with engine.connect() as connection:
        connection.execute(text("SET SESSION sort_buffer_size = 3145728;"))
        result = connection.execute(stmt)
        articles = result.fetchall()

    for article in articles:
    # for i in range(1):
        # article = articles[0]
        if article[4] == id:
            article_data = {
                'id': article[4],
                'Title': article[5],
                'Date': article[6],
                'Sources': [article[3]],
                'Sources_Short': [article[2]],  
                'Image': article[7],
                'Countries': article[0].split(',') if article[0] else [],
                'Countries_Code': article[1].split(',') if article[0] else [],
                'Type': article[8],
                'Text': article[9],
                'Link': article[10],
                'Map': json.loads(article[11])
            }
            return json.dumps(article_data)
    
    return json.dumps({'error': 'Article not found'})


if __name__ == "__main__":
    app.run(debug=True, port=9000)
