import json
import unittest
import requests
from urllib.parse import urlparse

class EndpointTestCase(unittest.TestCase):
    def setUp(self):
        self.BASE_URL = "https://api.supportsouthsudan.me"

    """ 
    GET ALL Countries
    """
    def test_countries_endpoint_status_code(self):
        response = requests.get(self.BASE_URL + "/countries")
        self.assertEqual(response.status_code, 200)

    def test_countries_endpoint_returns_countries(self):
        response = requests.get(self.BASE_URL + "/countries")
        data = response.json()
        self.assertIn('countries', data)
        self.assertIsInstance(data['countries'], list)

    def test_countries_endpoint_countries_structure(self):
        response = requests.get(self.BASE_URL + "/countries")
        data = response.json()
        countries = data['countries']
        if countries:
            first_country = countries[0]
            expected_keys = {
                'asylum_seekers', 'chart', 'code', 'distance', 'flag_url', 'name',
                'news_id', 'news_titles', 'org_names', 'org_short_names', 'population', 'refugees'
            }
            self.assertSetEqual(set(first_country.keys()), expected_keys)
            self.assertIsInstance(first_country['name'], str)
            self.assertIsInstance(first_country['code'], str)
            self.assertIsInstance(first_country['population'], int)

            self.assertTrue(first_country['distance'] >= 0) 
            self.assertIsInstance(first_country['news_id'], list)
            self.assertTrue(all(isinstance(id, str) for id in first_country['news_id'])) 
    """ 
    GET Individual Countries
    """
    def test_individual_country_endpoint_status_code(self):
        response = requests.get(self.BASE_URL + "/countries/sudan")
        self.assertEqual(response.status_code, 200)

    def test_individual_country_endpoint_returns_correct_country_info(self):
        response = requests.get(self.BASE_URL + "/countries/sudan")
        data = response.json()
        expected_keys = {
            'asylum_seekers', 'chart', 'code', 'distance', 'flag_url', 'name',
            'news_id', 'news_titles', 'org_names', 'org_short_names', 'population', 'refugees'
        }
        self.assertSetEqual(set(data.keys()), expected_keys)

        self.assertEqual(data['code'], "SDN")
        self.assertEqual(data['name'], "Sudan")

    def test_individual_country_endpoint_field_types(self):
        response = requests.get(self.BASE_URL + "/countries/sudan")
        data = response.json()
        self.assertIsInstance(data['asylum_seekers'], int)
        self.assertIsInstance(data['chart'], str)
        self.assertIsInstance(data['code'], str)
        self.assertIsInstance(data['distance'], float)
        self.assertIsInstance(data['flag_url'], str)
        self.assertIsInstance(data['name'], str)
        self.assertIsInstance(data['news_id'], list)
        self.assertTrue(all(isinstance(id, str) for id in data['news_id']))
        self.assertIsInstance(data['news_titles'], list)
        self.assertTrue(all(isinstance(title, str) for title in data['news_titles']))
        self.assertIsInstance(data['org_names'], list)
        self.assertTrue(all(isinstance(name, str) for name in data['org_names']))
        self.assertIsInstance(data['org_short_names'], list)
        self.assertTrue(all(isinstance(name, str) for name in data['org_short_names']))
        self.assertIsInstance(data['population'], int)

    """ 
    GET ALL News
    """
    def test_news_endpoint_status_code(self):
        response = requests.get(self.BASE_URL + "/news")
        self.assertEqual(response.status_code, 200)

    def test_news_endpoint_returns_news(self):
        response = requests.get(self.BASE_URL + "/news")
        data = response.json()
        self.assertIn('news', data)
        self.assertIsInstance(data['news'], list)

    def test_news_endpoint_news_structure(self):
        response = requests.get(self.BASE_URL + "/news")
        data = response.json()
        news_items = data['news']
        if news_items:
            first_news_item = news_items[0]
            expected_keys = {
                'Countries', 'Countries_Code', 'Date', 'Image', 'Link', 
                'Map', 'Sources', 'Sources_Short', 'Text', 'Title', 
                'Type', 'id'
            }
            self.assertSetEqual(set(first_news_item.keys()), expected_keys)
    
    """
    GET Individual News
    """
    def test_specific_news_endpoint_status_code(self):
        response = requests.get(self.BASE_URL + "/news/4038779")
        self.assertEqual(response.status_code, 200)

    def test_specific_news_endpoint_returns_correct_news(self):
        response = requests.get(self.BASE_URL + "/news/4038779")
        data = response.json()
        expected_keys = {
            'id', 'Title', 'Date', 'Sources', 'Sources_Short', 'Image', 
            'Countries', 'Countries_Code', 'Type', 'Text', 'Link', 'Map'
        }
        self.assertSetEqual(set(data.keys()), expected_keys)

        self.assertEqual(data['id'], 4038779)
        self.assertEqual(data['Title'], "Polio this week as of 14 February 2024")
        self.assertEqual(data['Countries'], "Afghanistan")

    def test_specific_news_endpoint_field_types(self):
        response = requests.get(self.BASE_URL + "/news/4038779")
        data = response.json()
        self.assertIsInstance(data['id'], int)
        self.assertIsInstance(data['Title'], str)
        self.assertIsInstance(data['Date'], str)
        self.assertIsInstance(data['Sources'], str)
        self.assertIsInstance(data['Sources_Short'], str)
        self.assertIsInstance(data['Image'], str)
        self.assertIsInstance(data['Countries'], str)
        self.assertIsInstance(data['Countries_Code'], str)
        self.assertIsInstance(data['Type'], str)
        self.assertIsInstance(data['Text'], str)
        self.assertIsInstance(data['Link'], str)
        self.assertIsInstance(data['Map'], list)

        if data['Map']:
            self.assertTrue(all(isinstance(i, int) for i in data['Map']))

    """
    GET ALL Orgs
    """
    def test_orgs_endpoint_status_code(self):
        response = requests.get(self.BASE_URL + "/orgs")
        self.assertEqual(response.status_code, 200)

    def test_orgs_endpoint_returns_orgs(self):
        response = requests.get(self.BASE_URL + "/orgs")
        data = response.json()
        self.assertIn('orgs', data)
        self.assertIsInstance(data['orgs'], list)

    def test_orgs_endpoint_orgs_structure(self):
        response = requests.get(self.BASE_URL + "/orgs")
        data = response.json()
        orgs = data['orgs']
        if orgs: 
            first_org = orgs[0]
            expected_keys = {
                'country', 'country_code', 'date_created', 'description',
                'homepage', 'id', 'image', 'name', 'news', 'news_id',
                'short_name', 'status', 'type'
            }
            self.assertSetEqual(set(first_org.keys()), expected_keys)

    """
    GET individual orgs
    """
    def test_org_individual_endpoint_structure(self):
        response = requests.get(self.BASE_URL + "/orgs/AGRA")
        self.assertEqual(response.status_code, 200)
        
        data = response.json()
        expected_keys = {'id', 'name', 'short_name', 'image', 'homepage', 'date_created', 
                         'status', 'type', 'description', 'country_code', 'news', 'news_id', 'country'}
        self.assertSetEqual(set(data.keys()), expected_keys)
    
    def test_org_individual_data_types(self):
        response = requests.get(self.BASE_URL + "/orgs/AGRA")
        self.assertEqual(response.status_code, 200)
        
        data = response.json()
        self.assertIsInstance(data['id'], int)
        self.assertIsInstance(data['name'], str)
        self.assertIsInstance(data['short_name'], str)
        self.assertIsInstance(data['image'], str)
        self.assertIsInstance(data['homepage'], str)
        self.assertIsInstance(data['date_created'], str)
        self.assertIsInstance(data['status'], str)
        self.assertIsInstance(data['type'], str)
        self.assertIsInstance(data['description'], str)
        self.assertIsInstance(data['country_code'], str)
        self.assertIsInstance(data['news'], str)
        self.assertIsInstance(data['news_id'], str)
        self.assertIsInstance(data['country'], str)

if __name__ == '__main__':
    unittest.main()