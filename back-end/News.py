import requests
import json
import time

south_sudan_iso = "SSD"
endpoints = ["reports"]
news_data = []
json_file_path = "./jsons/news_db.json"
# max_articles = 200 


page = 1  
results_per_page = 6

total_articles_retrieved = 0

while total_articles_retrieved < max_articles:

    response = requests.get(f"https://api.reliefweb.int/v1/reports?appname=apidoc&query[value]=South Sudan Refugee&query[fields][]=source&fields[include][]=source.shortname&fields[include][]=country.name&fields[include][]=primary_country.shortname&fields[include][]=date.created&fields[include][]=image&fields[include][]=headline.title&fields[include][]=theme.name&fields[include][]=format.name&page={page}&limit={results_per_page}")

    if response.status_code == 200:
        response_data = response.json()
        reports = response_data["data"]
        
        # No more reports, break the loop
        if not reports:
            break

        for report in reports:
            fields = report["fields"]
            
            # Check if primary country is "world", if yes, skip this article
            primary_country = fields.get("primary_country").get("shortname")
            if primary_country.lower() == "world":
                continue

            # Extract country information
            country_names = [country.get("name") for country in fields.get("country", [])]
 
            instance_template = {
                "Title": fields.get("title"),
                "Date": fields.get("date").get("created"),
                "Source": [source.get("shortname") for source in fields.get("source", [])],
                "Image": "",
                "Countries":country_names,
                'Type': fields.get("format")[0]["name"] if isinstance(fields.get("format"), list) else fields.get("format").get("name", "")
            }
            
            subscription_key = "e69b958dd5e9425ea4fd65e330139470"
            search_url = "https://api.bing.microsoft.com/v7.0/images/search"
            search_term = instance_template["Title"]
            headers = {"Ocp-Apim-Subscription-Key" : subscription_key}
            params = {"q": search_term, "license":"public", "count":"1", "offset":"0"}
            time.sleep(1)
            response = requests.get(search_url, headers=headers, params=params)
            response.raise_for_status()
            search_results = response.json()
            instance_template["Image"] = [img["thumbnailUrl"] for img in search_results["value"][:16]]
            print("thumbnailUrl")

            news_data.append(instance_template)
            total_articles_retrieved += 1

            if total_articles_retrieved >= max_articles:
                break  # Exit the loop if the maximum number of articles has been reached

        # Move to the next page
        page += 1
    else:
        print(f"Request Error: {response.status_code}")
        print(f"Request URL: {response.url}")
        print(f"Request Failure Reason: {response.request.reason}")
        break

# Write data to json file
with open(json_file_path, "w") as json_file:
    json.dump(news_data, json_file, indent=2)
