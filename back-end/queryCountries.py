import json

# Fetch all data from the Countries table
cursor.execute("SELECT * FROM Countries")
country_records = cursor.fetchall()

# list to store country data
country_data_list = []

# Iterate through each record and append to the country_data_list
for record in country_records:
    country_data = {
        "name": record[0],
        "code": record[1],
        "population": record[2],
        "refugees": record[3],
        "asylum_seekers": record[4],
        "distance": record[5],
        "flag_url": record[6],
        "chart": record[7]
    }
    country_data_list.append(country_data)

# output JSON file path
output_json_file = "countries_data.json"

with open(output_json_file, "w") as json_file:
    json.dump(country_data_list, json_file, indent=4)

cursor.close()